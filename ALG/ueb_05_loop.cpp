/**
    Loesung Aufgabe 4
	ueb_05_loop.cpp
    Purpose: Generate a Multiplication Table from 1 to 100 

	Matrikelnummer: 1352285

    @author Niklas Weimann
    @version 1.0 19.11.2017
*/
#include <iostream>
#include <string>

// This enables us to use all std definitions directly.
using namespace std;


/**
    Print a Multiplication Table
*/
void printTable()
{
	for(int i = 1; i <= 10; i++)
	{
		for(int j = 1; j <= 10; j++)
		{
			cout << i *j << " ";
		}
		cout << endl;
	}
}


int main(int argc, char* argv[])
{
	printTable();
	
	
	return 0;
}
