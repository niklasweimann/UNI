/**
    Loesung Aufgabe 3
	ueb_06_swapIt.cpp
    Purpose: Swap the Value of two integers

	Matrikelnummer: 1352285

    @author Niklas Weimann
    @version 1.0 21.11.2017
*/
#include <iostream>

using namespace std;

/** Vertausche die Werte, die in X und Y gespeichert 
	sind, indem die Pointer ausgetauscht werden
	@param x:int Wert, der getauscht werden soll
	@param y:int Wert, der getauscht werden soll
*/
void swapIt(int *x, int *y)
{
	int temp;
	temp = *x;
	*x = *y;
	*y = temp;
}

int main()
{
 int x=5;
 int y=10;

 cout << "Vorher: x= "<< x << " y = " << y << endl;

 swapIt (&x,&y);

 cout << "Nachher: x= "<< x << " y = " << y << endl;

 return 0;
}
