/**
    Loesung Aufgabe 3
	ueb_11_prim.cpp

	Matrikelnummer: 1352285

    @author Niklas Weimann
    @version 1.0 27.01.2018
*/
#include <iostream>
#include <string>
#include <limits>

using namespace std;

// N muss > 7 sein
#define N 7

/** Diese Funktion gibt einen Graphen G aus. */
void print(int G[N][N])
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			int k = G[i][j];
			if (k <= 0) k = 1;
			while (k < 1000)
			{
				cout << " ";
				k *= 10;
			}
			cout << G[i][j];
		}
		cout << "\n";
	}
}

/** Die Funktion berechnet den MST von G, 
	der in M gespeichert wird. 
	M wird in der main-Funktion initialisiert! */
void prim(int G[N][N], int M[N][N])
{
	//1. Schritt: Alle Knoten als besucht markieren
	int visited[N];

	for(int i = 0; i <= N; i++)
		visited[i] = 0;
	
	//2. Schritt: Wähle einen beliebigen Startknoten und markiere ihn als besucht
	visited[0] = 1;


	//3. Schritt: solange nicht alle Knoten besucht sind"
	for(int k = 1; k < N; k++)
	{
		int currenti = 0;
		int currentj = 0;
		int currentweight = std::numeric_limits<int>::max();

		for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {   
                //ﬁnde billie Kante e, die einen besuchten und einen nicht besuchten Knoten verbindet
                if ((visited[i]+visited[j] == 1) && (G[i][j] > 0) && (G[i][j] < currentweight))
                {

                    currenti = i;
                    currentj = j;
                    currentweight = G[i][j];
                }
            }
        }

        // trage Kante in M ein und markiere bislang unbesuchten Knoten als besucht
        visited[currenti] = visited[currentj] = 1;
        M[currenti][currentj] = currentweight;
        M[currentj][currenti] = currentweight;
	}
}

int main(int argc, char* argv[])
{
	// In den Adjazenzmatrizen entsprechen die Zahlen
	// den Kantengewichten/-kosten. Gewicht 0 bedeutet, 
	// dass keine Kante zwischen den Knoten existiert.	
	//               a   b   c   d   e   f   g
	int G[N][N] = {{ 0, 13,  0,  4,  5,  0,  0},  // a 
	               {13,  0, 19,  7,  0,  0, 24},  // b
	               { 0, 19,  0,  0,  0,  0, 23},  // c
	               { 4,  7,  0,  0, 10, 18,  0},  // d
	               { 5,  0,  0, 10,  0,  0,  0},  // e
	               { 0,  0,  0, 18,  0,  0,  3},  // f
	               { 0, 24, 23,  0,  0,  3,  0}}; // g
									
	//               a   b   c   d   e   f   g
	int M[N][N] = {{ 0,  0,  0,  0,  0,  0,  0},  // a 
	               { 0,  0,  0,  0,  0,  0,  0},  // b
	               { 0,  0,  0,  0,  0,  0,  0},  // c
	               { 0,  0,  0,  0,  0,  0,  0},  // d
	               { 0,  0,  0,  0,  0,  0,  0},  // e
	               { 0,  0,  0,  0,  0,  0,  0},  // f
	               { 0,  0,  0,  0,  0,  0,  0}}; // g
					
	cout << "Input graph G:\n";			 
	print(G);

	prim(G, M);
	cout << "Minimal Spanning Tree (Prim):\n";			 
	print(M);

	return 0;
}
