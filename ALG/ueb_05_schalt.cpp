/**
    Loesung Aufgabe 2
	ueb_05_schalt.cpp
    Purpose: Check Leap jahr or Not in C++

	Matrikelnummer: 1352285

    @author Niklas Weimann
    @version 1.0 19.11.2017
*/
#include <iostream>

using namespace std;

int main(int argc,  char** argv)
{
    int jahr = 2016;
	bool isLeapjahr = false;
	string output = "Error";

	if(jahr%4 == 0)
	{
		if(jahr%100 == 0)
		{
			if(jahr%400 == 0)
			{
				cout << "Schaltjahr!" << endl;
			}
			else
			{
				cout << "Kein Schaltjahr!" << endl;
			}
		}
		else
		{
			cout << "Schaltjahr!" << endl;
		}
	}
	else
	{
		cout << "Kein Schaltjahr!" << endl;
	}

    return 0;
}
