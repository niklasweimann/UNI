/**
    Loesung Aufgabe 2
	ueb_06_Helferlein.cpp
    Purpose: Encrypt a Text with a given key 

	Matrikelnummer: 1352285

    @author Niklas Weimann
    @version 1.0 21.11.2017
*/
#include <iostream>
#include <string>
#include <math.h>

using namespace std;

/** Gibt das Querprodukt einer Zahl zurück
    @param zahl:int Zahl, deren Querprodukt berechnet werden soll
    @returns Querprodukt der Zahl
*/
int querProdukt(int zahl)
{
    int num = zahl;
    int product = 1;
    for(int i = 1; i <= (int)(log10(zahl)+1); i++)
    {
        product *= num%10; //Wert der aktuellen Stelle multiplizieren
        num/=10; //trenne die letze Stelle ab
    }
    return product;
}
/** Gibt die Quersumme einer Zahl zurück
    @param zahl:int Zahl, deren Quersumme berechnet werden soll
    @returns Quersumme der Zahl
*/
int querSumme(int zahl)
{
    int num = zahl;
    int product = 0;
    for(int i = 1; i <= (int)(log10(zahl)+1); i++)
    {
        product += num%10; //Wert der aktuellen Stelle addieren
        num/=10; //trenne die letze Stelle ab
    }
    return product;
}

/** Prüft, für welche Zahlen in einem Intervall Quersumme und Querprodukt identisch sind
    @param start:int Untere Grenze des Intervalls
    @param ende:int Obere Grenze des Intervalls
    @returns Querprodukt der Zahl
*/
void produktGleichQuadratsumme(int start, int ende)
{
    cout << "Querprodukt und Quersumme sind bei folgenden Zahlen gleich:" << endl;
    for(int i = start; i <= ende; i++)
    {
        int quadratQuerSumme = querSumme(i)*querSumme(i);
        if(querProdukt(i) == quadratQuerSumme)
        {
            cout << i << " ";
        }
    }
    cout << endl;
}

/** Dies ist der Einstiegspunkt des Programms. */
int main(int argc, char* argv[]) {

    int myZahl=473;
    int qP=querProdukt(myZahl);
    int qS=querSumme(myZahl);
    

    cout <<"Querprodukt: " << qP << endl; //F�r das Bsp. myZahl=432 m�sste hier 24 rauskommen
    cout <<"Quersumme: " << qS << endl;
    produktGleichQuadratsumme(100, 9999);
}
