/**
    Loesung Aufgabe 1
	ueb_06_crypt.cpp
    Purpose: Encrypt a Text with a given key 

	Matrikelnummer: 1352285

    @author Niklas Weimann
    @version 1.0 21.11.2017
*/
#include <iostream>
#include <string>

using namespace std;

/* "Verschluesselt" einen String, indem jedes Zeichen 
	mit einem key ODER-Verknüpft wird
	@param text:string Text, der verschlüsselt werden soll
	@param key:string Key, mit dem der Text verschlüsselt werden soll
	@return Gibt einen verschlüsselten Key zurück
**/
string encrypt(string text, string key)
{
	string result = text;
	
	unsigned int resultLength = result.length();

	for (unsigned int i = 0; i < resultLength; i++)
	{
		result[i] = text[i] ^ key[i % key.length()];
	}

	return result;
}

/** Hier beginnt das Hauptprogramm */
int main(int argc, char* argv[])
{
	string text = "Dies ist irgendein Text.";
	string key  =  "irgendein Passwort";
	
	//Ihre Loesung
	cout << encrypt(text, key);
		
	return 0;
}
