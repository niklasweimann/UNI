/**
    Loesung Aufgabe 3
	ueb_07_Uhr.cpp
    Purpose: Berechnet den Winkel zwischen dem Minuten und dem Stunden Zeiger einer Uhr.

	Matrikelnummer: 1352285

    @author Niklas Weimann
    @version 1.0 27.11.2017
*/
#include <iostream>
#include <string>

using namespace std;

/**
	Gibt den Betrag einer Zahl zurück
	@param num:float Zahl, deren Betrag berechnet werden soll
	@return Der Betrag der übergebenen Zahl
*/
float betrag(float num)
{
	return (num>=0 ? num : -num);
}

/**
	Berechnet den kleineren Winkel zwischen den Zeigern einer Uhr
	@param Stunde:int Aktulle Stunde
	@param Minuten:int aktuelle Minute
	@return Kleinerer Winkel zwischen Stundenzeiger und Minutenzeiger
*/
float berechneWinkel(int Stunden, int Minuten)
{
	//für 24-h Format
	if(Stunden>12)
		Stunden -=12;
	//Pro Stunde bewegt sich der Stundenzeiger 30° und pro Minute 0.5°
	float stundenWinkel = Stunden*30+Minuten*0.5;
	//Pro Minute bewegt sich der Minutenzeiger um 6°
	float minutenWinkel = Minuten*6;
	float angle = betrag(minutenWinkel-stundenWinkel);
	//Kleineren Winkel zurückgeben
	if(angle > 180)
		return 360-angle;
	else
		return angle;
}

/** Hier beginnt das Hauptprogramm */
int main(int argc, char* argv[])
{
	int stunde = 0;
	int minute = 0;
	bool eingabeGueltig = false;
	do{
		cout<<"Stunden: ";
		if(cin>>stunde);	//Userinput Stunde
		cout<<endl;
		cout<<"Minuten: ";
		if(cin>>minute);	//Userinput Minuten
		cout<<endl;
		if((stunde >=0 && stunde <=24) && (minute >=0 && minute <=60)) //Pruefen, ob die EIngabe gültig ist
			eingabeGueltig = true;
		else
			cout<<"Eingabe ungueltig! Versuchen Sie es erneut."<<endl;
	}while(eingabeGueltig ==false);
	cout<<berechneWinkel(stunde, minute);
	return 0;
}
