/**
    Loesung Aufgabe 2
	ueb_07_Uhr.cpp
    Purpose: Eine einfache Version einer Bibliothek.

	Matrikelnummer: 1352285

    @author Niklas Weimann
    @version 1.0 27.11.2017
*/
#include <iostream>
#include <string>

using namespace std;

class Book
{
	string m_title;
	string m_author;
	string m_isbn;

public:

	/**
		Konstruktor der Klasse Book
	*/
	Book(string title,
	     string author,
	     string isbn)
	{
		setTitle(title);
		setAuthor(author);
		setISBN(isbn);
	}

	// Getter und Setter
	string getTitle()
	{
		return m_title;
	}
	string setTitle(string title)
	{
		m_title = title;
	}
	string getAuthor()
	{
		return m_author;
	}
	string setAuthor(string author)
	{
		m_author = author;
	}
	string getISBN()
	{
		return m_isbn;
	}
	string setISBN(string isbn)
	{
		m_isbn = isbn;
	}
};

class Student
{
	// array der geliehenen Buecher
	Book* m_book[8];
	
public:
	/**
	Konstruktor der Klasse Student.
	*/
	Student()
	{
		for(int i = 0; i < 8; i++)
		{
			m_book[i] = NULL;
		}
	}
	
	/**
	Destruktor der Klasse Student
	*/
	~Student()
	{
		for(int i = 0; i < 8; i++) {
            if(m_book[i] != NULL){
                delete m_book[i];
                m_book[i] = NULL;
            }
        }
	}

	/**
		Fügt ein Buch in den Speicher hinzu
		@params book:Book Buch, das in den Speicher hinzugefügt werden soll
	*/
	void addBook(Book& book)
	{
		bool saved = false;
		for(int i = 0; i < 8 && !saved; i++)
		{
			if(m_book[i] == NULL)
			{
				m_book[i] = new Book(book);
				saved = true;
			}
		}
		if(!saved)
		{
			cout << "Kein freier Speicherplatz mehr für dieses Buch" << endl;
		}
	}

	/**
		Entfernt das übergebene Buch aus dem Speicher
		@parm book:Book Buch, das aus dem Speicher entfernt werden soll
	*/
	void removeBook(Book& book)
	{
		bool deleted = false;
		for(int i = 0; i < 8 && !deleted; i++) 
		{
            if(m_book[i]->getISBN() == book.getISBN()) {
                delete m_book[i];
                m_book[i] = NULL;
                deleted = true;
            }
        }
		if(!deleted)
		{
			cout << "Das Buch konnte nicht geloescht werden." << endl;
		}
	}	

	/**
		Gibt eine Liste aller Bücher aus, die sich im Speicher befinden.
	*/
	void printBooks()
	{
		for(int i = 0; i < 8; i++)
		{
			if(m_book[i] != NULL ) {
                cout << "[Buch " << i << "] ";
                cout << "Title: " << m_book[i]->getTitle() << endl;
                cout << "Author: " << m_book[i]->getAuthor();
                cout << ", ISBN: " << m_book[i]->getISBN() << endl;
            }
		}
	}	
};

/** This is the entry point of the program. */
int main(int argc, char* argv[])
{
	cout << "----------------\n";;

	Book book_1("Introduction to Algorithms",
	            "Th. Cormen, Ch. Leiserson, R. Rivest", 
	            "0262531968");

	Book book_2("The C++ Programming Language",
	            "Bjarne Stroustrup", 
	            "0201700735");

	Book book_3("Grundkurs Informatik",
	            "H. Ernst", 
	            "3528257172");

	Student student;
	student.addBook(book_1);
	student.addBook(book_2);
	student.addBook(book_3);
	student.printBooks();
	
	cout << "----------------\n";;
	
	student.removeBook(book_2);
	student.printBooks();
	
	cout << "----------------\n";;

	return 0;
}
