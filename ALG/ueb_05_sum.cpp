/**
    Loesung Aufgabe 1
	ueb_05_sum.cpp
    Purpose: Sum of all Digits in a given range

	Matrikelnummer: 1352285

    @author Niklas Weimann
    @version 1.0 19.11.2017
*/
#include <iostream>

int main(int argc, char* argv[])
{
    const int a = 17;
    const int b = 52;

    int summe = 0;

    for (int i = a; i <= b; i++)
	{
		summe = summe + i;
	}

    std::cout << "Summe: " << summe;
    return 0;
}