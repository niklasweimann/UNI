/**
    Loesung Aufgabe 1
	ueb_11_hash.cpp

	Matrikelnummer: 1352285

    @author Niklas Weimann
    @version 1.0 27.01.2018
*/
// System header files
#include <iostream>
#include <string>

// Eine Listenstruktur
#include "ueb_11_hash_list.h"

using namespace std;

/** Die Klasse bildet eine Struktur, die integer-Schl�ssel auf string-Objekte abbildet und
Kollisionen durch verkettete Listen behandelt. */
class HashMap
{
public:

	/** Der Konstruktor 
	n ist die Gr��e des Arrays, das von der Hashfunktion benutzt wird. */
	HashMap(int n)
	{
		m_array = new List[n];
		m_size = n;
	}
	
	/** Der Destruktor */
	~HashMap()
	{
		for (int i = 0; i < m_size; i++)
			for (ListElem* e = m_array[i].getFirst(); e != NULL; e = e->getNext())
				delete e->getData();
		delete [] m_array;
	}
		
	/** Gibt die HashMap aus. */
	void print()
	{
		for (int i = 0; i < m_size; i++)
		{
			cout << "[" << i << "] ";
			for (ListElem* e = m_array[i].getFirst(); e != NULL; e = e->getNext())
				cout << "-> (" << e->getData()->key << ", "
				     << e->getData()->value << ") ";
			cout << "\n";
		}
	}

	/** Liefert den Wert zum angegebenen Key zur�ck */
	string get(int key)
	{
		/************************************************************************/
		/* Aufgabenteil a)                                                      */
		/************************************************************************/
		//bestimme, an welcher Stelle die Liste im Array liegt
		int i = key % m_size;

		// Suche das DataElem in der Liste
		ListElem* e = m_array[i].getFirst();
		while(e != NULL)
		{
			if(e->getData()->key == key)
				return e->getData()->value;
			e = e->getNext();
		}
		return "Key konnte nicht gefunden werden.";				
	}

	/** F�gt den Schl�ssel key mit der Zeichenkette value zur HashMap hinzu */
	void put(int key, string value)
	{
		/************************************************************************/
		/* Aufgabenteil a)                                                      */
		/************************************************************************/
		//Module des aus Key und der Größe des Arrays, 
		//um die Daten möglichest gleich im Array zu verteilen
		int i = key % m_size;
		//Füge ein neues DataElem mit dem key und value 
		//an der Stelle i in das Array ein
		m_array[i].append(new DataElem(key,value));
	}

private:

	/** 
		Ein Array, welches Listen von DataElems speichert. Siehe ueb_13_1_list.hpp */
	List* m_array;

	/** Die Gr��e des Arrays. */
	int m_size;
};


//Enumeration zur Unterscheidung zwischen den Sondierungsverfahren
enum Method {LINEAR, QUADRATIC, DOUBLE};

/** Die Klasse bildet eine Struktur, die integer-Schl�ssel auf string-Objekte abbildet und
dazu verschiedene Sondierungsverfahren benutzt. */
class NewHashMap
{
public:

	/** Der Konstruktor 
	n ist die Gr��e des Arrays, das von der Hashfunktion benutzt wird. 
	method ist das Sondierungsverfahren. */
	NewHashMap(int n, Method method)
	{
		/**************************************************************************/
		/* Aufgabenteil b)                                                        */
		/**************************************************************************/
		//Membervariablen initialisieren
		m_size = n;
		m_method = method;
		m_array = new DataElem*[m_size];
		//Speicherplatz allokieren etc.
		for(int i = 0; i < m_size; i++)
			m_array[i] = NULL;
	}

	/** Der Destruktor */
	~NewHashMap()
	{
		/**************************************************************************/
		/* Aufgabenteil b)                                                        */
		/**************************************************************************/
		//Speicherplatz freigeben etc.
		for(int i = 0; i < m_size; i++)
			if(m_array[i] != NULL)
				delete m_array[i];
		delete[] m_array;			
	}

	/** Gibt die HashMap aus. */
	void print()
	{
		for (int i = 0; i < m_size; i++)
			if (m_array[i] != NULL)
				cout << "(" << m_array[i]->key << ", "
				<< m_array[i]->value << ")\n";
			else
				cout << "(-, -)\n";
	}

	/** Liefert den Wert zum angegebenen Key zur�ck */
	string get(int key)
	{
		/**************************************************************************/
		/* Aufgabenteil b)                                                        */
		/**************************************************************************/
		//Element suchen
		//solange nicht gefunden, je nach Sondierungsmethode weitersuchen.
		//...	
		int i = key % m_size;
		int j = 1;
		while(m_array[i]->key != key)
		{
			if(j >= 100)
			{
				exit(1);
			}

			switch(m_method)
			{
				case LINEAR:
					i = (key+j) % m_size;
					break;
				case DOUBLE:
					i = (key + j * key) % m_size;
					break;
				case QUADRATIC:
					i = (key+j*j) % m_size;
					break;
			}
			j++;
		}
		return "Value?!"; //Muss ge�ndert werden.			
	}

	/** F�gt den Schl�ssel key mit der Zeichenkette value zur HashMap hinzu */
	void put(int key, string value)
	{
		/**************************************************************************/
		/* Aufgabenteil b)                                                        */
		/**************************************************************************/
		//Index berechnen
		//Wenn Kollision, dann je nach Sondierungsmethode weiter springen.
		//Wenn freier Platz gefunden: Einordnen.
		int i = key % m_size;
		int j = 0;

		while(m_array[i] != NULL)
		{
			if(i >= 100)
				exit(1);
			
			switch(m_method)
			{
				case LINEAR:
					i = (key+j) % m_size;
					break;
				case DOUBLE:
					i = (key + j*key) % m_size;
					break;
				case QUADRATIC:
					i = (key+ j*j) % m_size;
					break;
			}
			j++;
		}
		m_array[i] = new DataElem(key, value);
		
	}

private:

	/** Array, welches DataElems speichert */
	DataElem** m_array;

	/** Die Gr��e des Arrays */
	int m_size;

	/** Die gew�hlte Sondierungsmethode */
	Method m_method;
};



int main(int argc, char* argv[])
{
	// Anzahl der Elemente, die gespeichert werden sollen
	const int n = 16;

	// Array mit irgendwelchen keys...
	int keys[n]      = {17,     8,      23,       3,        2, 
	                    128,    256,      16,       9,        10, 
	                    4,      0,       99,     5,      7,    1};
											
	// Array mit irgendwelchen W�rtern...
	string values[n] = {"Baum", "Hase", "Wurzel", "Schnee", "Flasche", 
	                    "Gold", "Beamer", "Papier", "Schere", "Kreide", 
	                    "Buch", "Apfel", "Zahl", "Heft", "CD", "Kabel"};
	
	/************************************************************************/
	/* Aufruf Aufgabenteil a)                                               */
	/************************************************************************/
	// Das HashMap Objekt
	//HashMap map(8);                       

	/************************************************************************/
	/* Aufruf Aufgabenteil b)                                               */
	/************************************************************************/
	NewHashMap map(25, DOUBLE);         
	
	// Hier werden die Elemente in die HashMap gesteckt (key/value - Paare).
	for (int i = 0; i < n; i++)
		map.put(keys[i], values[i]);	
	
	// Print array to terminal;
	cout << "Map content: \n";
	map.print();

	return 0;
}
