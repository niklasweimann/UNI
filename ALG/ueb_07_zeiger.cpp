/**
    Loesung Aufgabe 1
	ueb_07_zeiger.cpp
    Purpose: Erstellt und löscht einen Pointer

	Matrikelnummer: 1352285

    @author Niklas Weimann
    @version 1.0 27.11.2017
*/
#include <iostream>
#include <string>

using namespace std;

/** Hier beginnt das Hauptprogramm */
int main(int argc, char* argv[])
{
	//Ihre Loesung
	int *zahl = new int;
	*zahl = 21;

	cout << "Inhalt Zeigervariable: " << *zahl << endl;

	delete zahl;
		
	return 0;
}
