/**
    Loesung Aufgabe 7
	ueb_09_fibo.cpp
    Purpose: interativer, linearer Fibonacci-Algorithmus.

	Matrikelnummer: 1352285

    @author Niklas Weimann
    @version 1.0 14.12.2017
*/
#include <iostream>

// This enables us to use all std definitions directly.
using namespace std;

/** This function returns a Fibonacci number. */
int fib(int n)
{
	if (n <= 0) 
		return 0;
	if (n == 1) 
		return 1;

	// Calculate Fibonacci Numbers
	int pre = 0;
    int cur = 1;
    int fib = 0;
    for (int i = 2; i <= n; ++i) 
    {
        fib = cur + pre;
        pre = cur;
        cur = fib;
    }
	return fib;
	 /*
        (n-1)*2 add-operations
        -> O(n)
    */
}

/** This is the entry point of the program. */
int main(int argc, char* argv[])
{
	int i = 42;
	cout << "Please wait...\n";
	cout << "fib[" << i << "] = " << fib(i) << "\n";
}
