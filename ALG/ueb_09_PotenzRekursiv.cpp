/**
    Loesung Aufgabe 5
	ueb_09_PotenzRekursiv.cpp
    Purpose: Potenzen mit Rekursion berechnen.

	Matrikelnummer: 1352285

    @author Niklas Weimann
    @version 1.0 14.12.2017
*/
#include <iostream>
#include <string>

using namespace std;

/** Vervollst�ndigen Sie die Methode berechnePotenz mit Hilfe von Rekursionen */
int berechnePotenz(int a, int n)
{
    //Prüfen, ob größer 0
	if (a <= 0 | n <= 0)
    return 0;

    if (n == 1)
        return a;

    if (n > 1)
        return a*berechnePotenz(a, n - 1);
}   

/** Dies ist der Einstiegspunkt des Programms. Hier soll nichts ver�ndert werden! */
int main(int argc, char* argv[]) {

    int a=0;
    int n=0;
    int ergebnis=0;

    cout << "Geben Sie einen Wert >0 fuer a ein: ";
    cin >> a;

    cout << "Geben Sie einen Wert >0 fuer n ein: ";
    cin >> n;

    ergebnis=berechnePotenz(a,n);
    cout << "Ergebnis: " << ergebnis << endl;

return 0;
}
