/*
 * Created on Sun May 27 2018 at 21:52:46
 * @author Niklas Weimann
 * Matrikelnummer: 1352285
 */
class Alter {

    public static String gibBezeichnung(int Alter) {
        if (Alter > 0 && Alter < 18)
            return "Du bist ja noch ein Kind!";
        else if (Alter >= 18 && Alter < 67)
            return "Du stehst mitten im Leben!";
        else if (Alter >= 67)
            return "Als Rentner hat man es gut!";
        return "Mit Deinem Alter Stimmt was nicht!";
    }

    public static void main(String args[]) {
        System.out.println(gibBezeichnung(44));
    }
}