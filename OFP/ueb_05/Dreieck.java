/*
 * Created on Sun May 27 2018 at 21:53:8
 * @author Niklas Weimann
 * Matrikelnummer: 1352285
 */
public class Dreieck {

	public static void showPyramid(int rows) {
		// Hier steht Ihre Loesung
		for (int currentRow = 0; currentRow < rows; currentRow++) {
			for (int spaces = 0; spaces < rows - currentRow; spaces++) {
				System.out.print(" ");
			}
			for (int kreuze = 0; kreuze <= currentRow; kreuze++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}

	/**
	 * public static void showPyramid(int rows) { // Hier steht Ihre Loesung String
	 * result = ""; for (int currentRow = 0; currentRow < rows; currentRow++) { String row = ""; row += new
	 * String(new char[rows - currentRow - 1]).replace("\0", " "); row += new String(new
	 * char[2 * currentRow + 1]).replace("\0", "*");
	 * 
	 * result += row + "\n"; } System.out.print(result); }
	 */

	public static void main(String[] args) {
		int rowMax = 5; // Hoehe der Pyramide
		showPyramid(rowMax);
	}
}
