/*
 * Created on Sun May 27 2018 at 21:52:55
 * @author Niklas Weimann
 * Matrikelnummer: 1352285
 */
public class Discount {

    public static String berechneRabatt(double betrag) {

        if (betrag > 1000) {
            betrag = betrag - (betrag * 0.08);
            return (String.format("Es wurde ein Rabatt gewährt! Zu zahlender Betrag: %.2f EUR", betrag));
        }
        return (String.format("Zu zahlender Betrag: %.2f EUR", betrag));
    }

    public static void main(String[] args) {
        double rechnungsBetrag = 0.0; // Rechnungsbetrag

        rechnungsBetrag = Double.parseDouble(args[0]); // Kommandozeilenargument als Gleit-
                                                       // kommazahl auswerten (Umwandlung in
                                                       // eine double-Zahl und

        System.out.println(berechneRabatt(rechnungsBetrag));

    }
}