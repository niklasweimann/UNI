/*
 * Created on Sun May 27 2018 at 21:53:15
 * @author Niklas Weimann
 * Matrikelnummer: 1352285
 */
import java.io.*;

class Helferlein {
    public int max(int a, int b) {
        // Hier steht Ihre Loesung
        return (a > b) ? a : b;
    }
}

public class Starter {
    public static void main(String[] args) {
        Helferlein h = new Helferlein();
        System.out.println("Ausgabe: " + h.max(12, 43));
    }
}