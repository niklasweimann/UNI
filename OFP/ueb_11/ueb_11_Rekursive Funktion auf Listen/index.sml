fun index(i, xs) =
    if i < 0 then
        raise Subscript
    else
        case xs of
            [] => raise Subscript
            | (x::xs') => if i=0 then x else index(i-1, xs')