import java.io.*;

class TastaturIn {
	private InputStreamReader is=new InputStreamReader(System.in);
	private BufferedReader eingabe=new BufferedReader(is);
	
	public int readInt() throws IOException {
		String zeile;
		int wert=0;
		
		zeile=eingabe.readLine();
		wert=(new Integer(zeile)).intValue();
		
		return wert;
	}
	
	public String readString() throws IOException{
		String zeile="";
		
		zeile=eingabe.readLine();
		
		return zeile;
	}
}
