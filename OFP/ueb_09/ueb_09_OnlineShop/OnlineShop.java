﻿import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * OnlineShop
 */
public class OnlineShop {
    private Kundenverwaltung mKundenverwaltung;
    private Produktverwaltung mProduktverwaltung;
    private TastaturIn mTastaturIn;
    private HashMap<Kunde, Bestellung> mBestellungen = new HashMap<Kunde, Bestellung>();

    public OnlineShop() {
        mKundenverwaltung =  new Kundenverwaltung();
        mProduktverwaltung = new Produktverwaltung();
        mTastaturIn = new TastaturIn();
        showMainmenu();
    }
    
    private int readInput() {
        int eingabe = -1;
        try {
            eingabe = mTastaturIn.readInt();
        } catch (IOException e) {
            System.out.println("Fehler bei der Eingabe. Fehlermeldung: " + e.getMessage());
        } catch (NumberFormatException e) {
            System.out.println("Bitte geben Sie eine Zahl ein. Fehlermeldung: " + e.getMessage());
        }
        return eingabe;
    }
    
    private void showProducts() {
        System.out.println("---Produktliste---");
        int index = 0;
        ArrayList<Produkt> mProdukte = mProduktverwaltung.getProdukte();
        for (Produkt produkt : mProdukte) {
            System.out.println((index++) + " " + produkt.toString());
        }
    }

    private void showCustomers() {
        System.out.println("---Kundenliste---");
        LinkedList<Kunde> mKunden = mKundenverwaltung.getKunden();
        int index = 0;
        for (Kunde kunde : mKunden) {
            System.out.println((index++) + " " + kunde.toString());
        }
    }
    
    private void order() {
        this.showCustomers();
        System.out.print("Kundennummer?: ");
        int input = readInput();
        if (input >= 0 && input < mKundenverwaltung.getKunden().size())
        {
            orderProduct(mKundenverwaltung.getKunden().get(input));
        }
        else{
            order();
        }
    }

    private void orderProduct(Kunde pKunde) {
        this.showProducts();
        System.out.println("Produkt?: ");
        int input = readInput();
        if (input >= 0 && input < mProduktverwaltung.getProdukte().size()) {
            specifyQuantity(pKunde, mProduktverwaltung.getProdukte().get(input));
        } else {
            orderProduct(pKunde);
        }
    }
    
    private void specifyQuantity(Kunde pKunde, Produkt pProdukt) {
        Kunde mKunde = pKunde;
        Produkt mProdukt = pProdukt;
        System.out.println("Menge?: ");
        int input = readInput();
        if (input >= 0) {
            mBestellungen.put(mKunde, new Bestellung(mKunde, mProdukt, input));
            showMainmenu();
        } else {
            System.out.println("Bitte geben Sie eine gültige Menge ein.");
            specifyQuantity(pKunde, pProdukt);
        }
    }
    
    private void sendOrder() {
        this.showCustomers();
        int input = readInput();
        if (input >= 0 && input < mKundenverwaltung.getKunden().size()) {
            Kunde kunde = mKundenverwaltung.getKunden().get(input);

            Bestellung bestellung = mBestellungen.get(kunde);
            if (bestellung != null) {
                Produkt produkt = bestellung.getProdukt();
                System.out.println("Die Bestellung von " + kunde.getName() + ", " + produkt.getProduktname() + " für " +
                         bestellung.getPrice() + " EUR wurde an die " + kunde.getAnschrift() + " aus geliefert.");
                mBestellungen.remove(kunde);
                showMainmenu();
            }
            else{
                System.out.println("Fuer diesen Kunden wurde leider keine gültige Bestellung gefunden.");
                showMainmenu();
            }
        }
        else {
            System.out.println("Bitte geben Sie eine gültige Kundennummer an.");
            sendOrder();
        }
    }

    public void showMainmenu() {
        System.out.println("Online-Shop Wunschshop");
        System.out.println("-----------------------------");
        System.out.println("1 - Produkte anzeigen");
        System.out.println("2 - Bestellung aufgeben");
        System.out.println("3 - Bestellung abschicken");
        System.out.println("0 - Programm beenden");
        switch (readInput()) {
        case 0:
            this.close();
            break;
        case 1:
            this.showProducts();
            System.out.print("\n");
            this.showMainmenu();
            break;
        case 2:
            this.order();
            break;
        case 3:
            this.sendOrder();
            break;
        default:
            this.showMainmenu();
            break;
        }
    }

    private void close() {
        System.exit(1);
    }

    public static void main(String[] args) {
        OnlineShop mOnlineShop = new OnlineShop();
    }
}