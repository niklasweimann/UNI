/**
 * Produkt
 */
public class Produkt {
    String mProduktname;
    double mPreis;

    public Produkt(String pProduktname, double pPreis) {
        this.mPreis = pPreis;
        this.mProduktname = pProduktname;
    }

    /**
     * @return the mPreis
     */
    public double getPreis() {
        return mPreis;
    }

    /**
     * @return the mProduktname
     */
    public String getProduktname() {
        return mProduktname;
    }

    /**
     * @param mPreis the mPreis to set
     */
    public void setPreis(double mPreis) {
        this.mPreis = mPreis;
    }

    /**
     * @param mProduktname the mProduktname to set
     */
    public void setProduktname(String mProduktname) {
        this.mProduktname = mProduktname;
    }

    @Override
    public String toString() {
        return this.mProduktname + ", " + this.mPreis;
    }
}