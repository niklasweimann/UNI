﻿import java.io.*;
import java.util.ArrayList;

/**
 * Produktverwaltung
 */
public class Produktverwaltung {
    ArrayList<Produkt> mProdukte = new ArrayList<Produkt>();

    public Produktverwaltung() {
        mProdukte.add(new Produkt("Südwein", 24.99));
        mProdukte.add(new Produkt("Edelburgunder", 129.99));
        mProdukte.add(new Produkt("Speisekäse", 3.49));
        mProdukte.add(new Produkt("Erdammer", 5.99));
        mProdukte.add(new Produkt("Trüffel", 1499.00));
    }

    public void writeProdukte() {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream("Produkte.ser");
            oos = new ObjectOutputStream(fos);
            oos.writeObject(mProdukte);
        } catch (Exception e) {
            System.out.println("Fehler beim exportieren der Produkte aufgetreten. Fehlermeldung: " + e.getMessage());
        } finally {
            try {
                fos.close();
                oos.close();
            } catch (Exception e) {
                System.out.println("Fehler beim schließen der Streams. Fehlermeldung: " + e.getMessage());
            }
        }
    }

    public void readProdukte() {
        ObjectInputStream ois = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("Produkte.ser");
            ois = new ObjectInputStream(fis);
            mProdukte = (ArrayList<Produkt>) ois.readObject();
        } catch (Exception e) {
            System.out.println("Fehler beim lesen der Produkte. Fehlermeldung: " + e.getMessage());
        } finally {
            try {
                fis.close();
                ois.close();
            } catch (Exception e) {
                System.out.println("Fehler beim schließen der Streams. Fehlermeldung: " + e.getMessage());
            }
        }
    }

    public ArrayList<Produkt> getProdukte() {
        return mProdukte;
    }
}