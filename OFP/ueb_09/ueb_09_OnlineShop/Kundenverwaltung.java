﻿import java.io.*;
import java.util.LinkedList;

/**
 * Produktverwaltung
 */
public class Kundenverwaltung {
    LinkedList<Kunde> mKunden = new LinkedList<Kunde>();

    public Kundenverwaltung() {
        mKunden.add(new Kunde("Herbert Maier", "Kreutzweg 28a, 57072 Siegen"));
        mKunden.add(new Kunde("Heidi Schiffer", "Wellnesgasse 4, 13579 Schönhausen"));
        mKunden.add(new Kunde("Claudia Klum", "Wellnesgasse 4, 13579 Schönhausen"));
        mKunden.add(new Kunde("Peter Held", "Katzenweg 5, 98123 Frömmersbach "));
    }

    /**
     * @return the mKunden
     */
    public LinkedList<Kunde> getKunden() {
        return mKunden;
    }
}