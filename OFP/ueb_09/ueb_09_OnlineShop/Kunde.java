/**
 * Kunde
 */
public class Kunde {
    String mName;
    String mAnschrift;

    public Kunde(String pName, String pAnschrift) {
        this.mName = pName;
        this.mAnschrift = pAnschrift;
    }

    /**
     * @return the mAnschrift
     */
    public String getAnschrift() {
        return mAnschrift;
    }

    /**
     * @return the mName
     */
    public String getName() {
        return mName;
    }

    /**
     * @param mAnschrift the mAnschrift to set
     */
    public void setAnschrift(String mAnschrift) {
        this.mAnschrift = mAnschrift;
    }

    /**
     * @param mName the mName to set
     */
    public void setName(String mName) {
        this.mName = mName;
    }

    @Override
    public String toString() {
        return this.mName + ", " + this.mAnschrift;
    }
}