/**
 * Bestellung
 */
public class Bestellung {

    private Kunde mKunde;
    private Produkt mProdukt;
    private int mMenge;

    public Bestellung(Kunde pKunde, Produkt pProdukt, int pMenge) {
        this.mMenge = pMenge;
        this.mKunde = pKunde;
        this.mProdukt = pProdukt;
    }

    public double getPrice() {
        return Math.round(100.0 * (mProdukt.getPreis() * mMenge)) / 100.0;
    }

    /**
     * @param mKunde the mKunde to set
     */
    public void setKunde(Kunde pKunde) {
        this.mKunde = pKunde;
    }

    /**
     * @param mMenge the mMenge to set
     */
    public void setMenge(int pMenge) {
        this.mMenge = pMenge;
    }

    /**
     * @param mProdukt the mProdukt to set
     */
    public void setProdukt(Produkt pProdukt) {
        this.mProdukt = pProdukt;
    }

    /**
     * @return the mKunde
     */
    public Kunde getKunde() {
        return mKunde;
    }

    /**
     * @return the mMenge
     */
    public int getMenge() {
        return mMenge;
    }

    /**
     * @return the mProdukt
     */
    public Produkt getProdukt() {
        return mProdukt;
    }

    public Bestellung getBestellung() {
        return this;
    }
}