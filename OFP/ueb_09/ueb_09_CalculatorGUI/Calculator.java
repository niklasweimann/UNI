import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Calculator extends JFrame {

    Calculator() {
        JFrame window = new JFrame("Taschenrechner");
        window.setSize(400, 200);
        window.setDefaultCloseOperation(EXIT_ON_CLOSE);

        Container cont = window.getContentPane();

        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();

        JTextField Textfeld1 = new JTextField(10);
        JTextField Textfeld2 = new JTextField(10);
        JTextField TextfeldErgebnis = new JTextField(23);

        JButton reset = new JButton("Clear");
        JButton plus = new JButton("+");
        JButton mal = new JButton("*");

        reset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Textfeld1.setText("");
                Textfeld2.setText("");
                TextfeldErgebnis.setText("");
            }
        });

        plus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double[] numbers = getNumbers(Textfeld1, Textfeld2);
                TextfeldErgebnis.setText(add(numbers[0], numbers[1]));
            }
        });

        mal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double[] numbers = getNumbers(Textfeld1, Textfeld2);
                TextfeldErgebnis.setText(multiply(numbers[0], numbers[1]));
            }
        });

        panel1.add(Textfeld1);
        panel1.add(plus);
        panel1.add(mal);
        panel1.add(Textfeld2);
        
        panel2.add(TextfeldErgebnis);
        panel2.add(reset);
        

        cont.add(panel1, BorderLayout.NORTH);
        cont.add(panel2, BorderLayout.CENTER);

        window.setVisible(true);
    }

    public double[] getNumbers(JTextField pTextfeld1, JTextField pTextfeld2) {
        double[] result = new double[2];
        // Feld 1
        try {
            result[0] = Double.parseDouble(pTextfeld1.getText());
        } catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Falsche Eingabe im 1. Feld.", "Fehler", JOptionPane.WARNING_MESSAGE);
        }

        // Feld 2
        try {
            result[1] = Double.parseDouble(pTextfeld1.getText());
        } catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Falsche Eingabe im 2. Feld.", "Fehler", JOptionPane.WARNING_MESSAGE);
        }
        return result;
    }

    public String add(double pNumberA, double pNumberB) {
        return Double.toString(pNumberA + pNumberB);
    }

    public String multiply(double pNumberA, double pNumberB) {
        return Double.toString(pNumberA * pNumberB);
    }

    public static void main(String[] args) {
        Calculator mCalculator = new Calculator();
    }
}
