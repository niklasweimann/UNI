/*
 * Created on Wed May 30 2018 at 14:10:11
 * @author Niklas Weimann
 * Matrikelnummer: 1352285
 */

public class VektorWork {
    public static void main(String[] args) {
        int[] a = {12, -2, 23, 4, -15};
        int[] b = {8, 1, -7, 2, 23};
        int [] result = VektorWork.addVektor(a, b);
        for (int j=0; j < result.length; j++)
            System.out.println(result[j]);
    } 
    public static int[] addVektor(int[] a, int[] b)
    {
        int[] c = new int[a.length]; //austauschen gegen Array mit der Größe von a
        for(int i = 0; i < a.length; i++)
            c[i] = a[i] + b[i];
        return c;
    }
}