/*
 * Created on Wed May 30 2018 at 14:9:59
 * @author Niklas Weimann
 * Matrikelnummer: 1352285
 */
import java.io.*;
import java.lang.Integer;
public class Rest{
    public static void main(String args[]) {
        int i=Integer.parseInt(args[0]);
        int j=Integer.parseInt(args[1]);
        int ergebnis = i%j;

        switch (ergebnis) {
            case 0: System.out.println("Der Rest ist null"); break; //Teilen ohne Rest möglich
            case 1: System.out.println("Der Rest ist ungerade"); break;
            case 2: System.out.println("Der Rest ist eine einstelllige Primzahl"); break;        
            default:
                if(ergebnis%2==1)
                {
                    if(ergebnis<=7)
                        System.out.println("Der Rest ist eine einstellige Primzahl");
                    else
                        System.out.println("Der Rest ist ungerade");
                }
                else 
                    System.out.println("Keine der Aussagen trifft zu");
                break;
        }
    }
}