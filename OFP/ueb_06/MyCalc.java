/*
 * Created on Wed May 30 2018 at 14:9:49
 * @author Niklas Weimann
 * Matrikelnummer: 1352285
 */
import java.io.*;
class CalcIt {
    public int ggT (int a, int b) {
        //Ihre Loesung
        int m = a;
        int n = b;
        int r = -1;
        
        while(r != 0)
        {
            if(m<n) // n und m tauschen
            {
                int temp = m;
                m = n;
                n = temp;
            }
            r = m-n;
            m = n;
            n = r;
            if (r == 0)
                break;
        }
        return m; //Ergebnis der Methode ist m  
    }
}
public class MyCalc {
    public static void main(String[] args) {
        CalcIt c = new CalcIt();
        System.out.println("Ausgabe: "+c.ggT(48,32));
    }
}