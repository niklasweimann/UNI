/*
 * Created on Wed May 30 2018 at 14:9:41
 * @author Niklas Weimann
 * Matrikelnummer: 1352285
 */
import java.util.Arrays;

public class ArrayWork {
    public static double ArrayFunc(double[] w, int func)
    {
    //Ihre Loesung  
    Arrays.sort(w); //Array sortieren
    double[] sorted = w;
        switch (func) {
            case 1:
                return sorted[0];
            case 2:
                return sorted[sorted.length-1];
            case 3:
                return sorted[(sorted.length-1)/2];
            case 4:
                double sum = 0;
                for(int i = 0; i < sorted.length; i++)
                    sum += sorted[i];
                return sum * 1.19;
            default:
                return 0;
        }
    }
    public static void main(String[] args) {
        int arg=Integer.parseInt(args[0]);
        double werte[] = {1.9, 4.6, 99.0, 12.49, 78.99, 0.5, 56.98, 8.90, 119.90, 2.20};
        System.out.println(ArrayWork.ArrayFunc(werte, arg));
    }
}