public class Datum extends Zeit implements java.io.Serializable{
    private int jahr;
    private int monat;
    private int tag;

    public Datum(int pTag, int pMonat, int pJahr, int pStunde, int pMinute) {
        super(pStunde, pMinute);
        this.tag = pTag;
        this.monat = pMonat;
        this.jahr = pJahr;
    }
    
    public String toString() {
        return tag + "." + monat + "." + jahr + " " + super.toString();
    }
}