import java.io.*;
public class Testzeit {

    public static void main(String[] args) {
        
        Datum mDatum = new Datum(15, 7, 2012, 12, 12);
        
        // Serialize
        try {
            FileOutputStream mFileOutputStream = new FileOutputStream("IO.txt");
            ObjectOutputStream mObjectOutputStream = new ObjectOutputStream(mFileOutputStream);
            mObjectOutputStream.writeObject(mDatum);
            mObjectOutputStream.close();
            mFileOutputStream.close();
        } catch (IOException i) {
            i.printStackTrace();
        }

        // Deserialize
        mDatum = null;
        try {
            FileInputStream mFileInputStream = new FileInputStream("IO.txt");
            ObjectInputStream mObjectInputStream = new ObjectInputStream(mFileInputStream);
            mDatum = (Datum) mObjectInputStream.readObject();
            mObjectInputStream.close();
            mFileInputStream.close();
        } catch (IOException i) {
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("Class not found");
            c.printStackTrace();
            return;
        }

        System.out.println("Deserialized Datum: " + mDatum.toString());

    }
}