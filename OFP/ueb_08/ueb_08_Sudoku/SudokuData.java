interface SudokuObserver {
	public void modified(int i, int j);
}

public class SudokuData
{
	private int[][] feld = new int[9][9];
	private SudokuObserver obs = null;

	public SudokuData()
 	{
		int i,j;
		for (i=0; i<9; i++) {
			for (j=0; j<9; j++) {
				feld[i][j] = 0;
			}
		}
	}

	public int getNumber(int x, int y)
	{
		return feld[x][y];
	}

	public void setNumber(int x, int y, int v)
	{
		feld[x][y] = v;
		if (obs != null)
			obs.modified(x, y);
	}

	public void setObserver(SudokuObserver o)
	{
		obs = o;
	}
}
