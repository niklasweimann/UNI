import java.awt.List;
import java.util.*;

public class SudokuSolver {
    SudokuData data;

    public SudokuSolver(SudokuData d) {
        data = d;
    }

    // offen(x,y) berechnet fuer das Feld (x,y) die Menge an offenen Kandidaten
    public java.util.List<Integer> offen(int x, int y) {
        /*
         * Pseudoalgorithmus:
         */
        int posX = x;
        int posY = y;
        // - Fuege die Zahlen 1-9 einer Collection res hinzu.
        ArrayList<Integer> res = new ArrayList<Integer>();
        for (int i = 1; i <= 9; i++)
            res.add(i);

        // - Schaue in Zeile x nach, welche Zahlen bereits vergeben sind und loesche
        // diese aus der Collection res
        for (int i = 0; i < 9; i++) {
            if (res.contains(new Integer(data.getNumber(i, posY))))
                res.remove(new Integer(data.getNumber(i, posY)));
        }

        // - Schaue in Spalte y nach, welche Zahlen bereits vergeben sind und loesche
        // diese aus der Collection res
        for (int i = 0; i < 9; i++) {
            if (res.contains(new Integer(data.getNumber(posX, i))))
                res.remove(new Integer(data.getNumber(posX, i)));
        }
        // - Schaue in dem Kasten zu Feld (x,y) welche Zahlen bereits vergeben sind und
        // loesche diese aus der Collection.
        int quadStartX = (int) (posX / 3) * 3;
        int quadStartY = (int) (posY / 3) * 3;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                res.remove(new Integer(Math.abs(data.getNumber(i + quadStartX, j + quadStartY))));
            }
        }
        // - Gib die Menge der Uebriggebliebenen Kandidaten zurueck.
        return res;
    }

    // findSolution() berechnet eine Loesung mittels Backtracking
    public boolean findSolution() {
        // Eine moegliche Strategie:
        // - Finde zuerst das Feld, das die wenigsten offenen Kandidaten hat
        //  - Falls mehrere Felder die gleiche Anzahl offener Kandidaten haben, dann waehle irgendeines dieser Feld aus.
        //  - Falls es keine offenen Kandidaten mehr gibt, ist eine Loesung berechnet. In diesem Fall gib true zurueck
        // - Durchlaufe nun die Kandidaten dieses Feldes in dem der erstbeste Kandidat ausgewaehlt und in das Feld[x][y] gesetzt wird.
        // - Rufe nun die Methode findSolution() auf (Rekursion!) um in Abhaengigkeit des gesetzten Wertes die weiteren Felder zu berechnen.
        //  Falls die weiteren Berechnungen in eine Sackgasse fuehren, nimm den naechsten moeglichen Kandidaten (Backtracking!)
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                if (data.getNumber(row, col) != 0) {
                    continue;
                }
                for (int i = 0; i < offen(row, col).size(); i++) {
                    data.setNumber(row, col, (int) offen(row, col).toArray()[i]);

                    if (findSolution()) {
                        return true;
                    } else {
                        data.setNumber(row, col, 0);
                    }
                }
                return false;
            }
        }
        return true;
    }
}
