import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class SudokuGUI implements SudokuObserver
{
	private JTextField[][] tf = new JTextField[9][9];
	private SudokuSolver solver;
	private SudokuData data;

	public SudokuGUI(SudokuSolver solv, SudokuData dat)
 	{
		JFrame win = new JFrame("Sudoku");
		Container cont = win.getContentPane();
		int i,j;

		solver = solv;
		data = dat;

		data.setObserver(this);

		JPanel panel1 = new JPanel();
		cont.add(panel1,BorderLayout.CENTER);
		panel1.setLayout(new GridLayout(9,9));
		for (i=0; i<9; i++) {
			for (j=0; j<9; j++) {
				tf[i][j] = new JTextField("",2);
				panel1.add(tf[i][j]);
				modified(i, j);
			}
		}

		JPanel panel2 = new JPanel();
		cont.add(panel2, BorderLayout.SOUTH);

		JButton but = new JButton("Start");
		but.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					updateData();
					solver.findSolution();
				}
			});
		panel2.add(but);

		JButton clr = new JButton("Clear");
		clr.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					clear();
				}
			});
		panel2.add(clr);

		JButton exit = new JButton("Exit");
		exit.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.exit(0);
				}
			});
		panel2.add(exit);

		win.pack();

		// Schlie�en des Fensters ==> Programmende
		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Fenster am Bildschirm zeigen
		win.setVisible(true);
	}

	public void modified(int x, int y)
	{
		int v = data.getNumber(x, y);
		if (v == 0)
			tf[x][y].setText("");
		else
			tf[x][y].setText(""+v);
	}

	void updateData()
	{
		int i,j;
		for (i=0; i<9; i++) {
			for (j=0; j<9; j++) {
				String s = tf[i][j].getText();
				if (s.equals(""))
					data.setNumber(i, j, 0);
				else
					data.setNumber(i, j, Integer.parseInt(s));
			}
		}
	}

	void clear()
	{
		int i,j;
		for (i=0; i<9; i++) {
			for (j=0; j<9; j++) {
				tf[i][j].setText("");
				data.setNumber(i, j, 0);
			}
		}
	}
	
	public static void main(String args[])
 	{
		SudokuData data = new SudokuData();
		SudokuSolver solv = new SudokuSolver(data);
		SudokuGUI gui = new SudokuGUI(solv, data);
	}
}
