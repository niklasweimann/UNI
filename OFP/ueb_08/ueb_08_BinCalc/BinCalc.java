import java.io.*;

class TastaturIn {
	private InputStreamReader is=new InputStreamReader(System.in);
	private BufferedReader eingabe=new BufferedReader(is);
	
	//Liest einen Wert von der Konsole ein 
	//und gibt diesen als int-Wert zurück  
	public int readInt() throws IOException, NumberFormatException {
		String zeile;
		int wert=0;
		
		zeile=eingabe.readLine();
		wert=(new Integer(zeile)).intValue(); //für Java < Version 9
		//wert = Integer.parseInt(zeile); //für Java > Version 9
		
		return wert;
	}
	
	//Liest einen Wert von der Konsole ein 
	//und gibt diesen als String zurück 
	public String readString() throws IOException, NumberFormatException{
		String zeile="";
		
		zeile=eingabe.readLine();
		
		return zeile;
	}
}


public class BinCalc {
	public static String Dec2Bin(int dec) {
		//Ihre Loesung
		String binary = "";
		while (dec != 0) {
			int d = dec % 2;
			binary = Integer.toString(d) + binary;
			dec /= 2;
		}
		return binary;

	}
	
	public static void main(String args[]) {
		//Ihre Loesung
		System.out.println("Bitte geben Sie eine Zahl ein: ");
		boolean valid = false;
		String result = "";
		int num = 0;
		while (!valid) {
			try {
				TastaturIn in = new TastaturIn();
				num = in.readInt();
				valid = true;
			} catch (Exception e) {
				System.out.println("Fehler! Keine int-Zahl!");
			}
		}
		result = Dec2Bin(num);
		System.out.println("Die binaere Represenation von " + num + " lautet: " + result);
	}
}
