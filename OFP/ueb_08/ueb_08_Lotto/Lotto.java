import java.awt.List;
import java.util.*;

public class Lotto {
    public static Collection<Integer> tippen () {
        Collection<Integer> lottotipp = new ArrayList<Integer>();
        while (lottotipp.size() < 6) {
            int newRandom = (int) (Math.random() * 49 + 1);
            if (!lottotipp.contains(newRandom))
                lottotipp.add((int) newRandom);
        }
        return new TreeSet<Integer>(lottotipp); 
    }

    public static void main(String[] args) {
        ArrayList<Integer> zahlen = new ArrayList<Integer>(tippen());
        System.out.println("Ihr Tipp: "+zahlen); 
    }
}