/*
 * Created on Sun Jun 10 2018 at 23:4:21
 * @author Niklas Weimann
 * Matrikelnummer: 1352285
 */
/**
 * Angestellter
 */
public class Angestellter {
    double stdProMonat;
    double geldProStunde;

    public Angestellter() {
        this.geldProStunde = 0;
        this.stdProMonat = 0;
    }

    public Angestellter(double arbeitstdProMonat, double gehaltProStunde){
        this.stdProMonat = arbeitstdProMonat;
        this.geldProStunde = gehaltProStunde;
    }

    public double berechneMonatseinkommen() {
        return stdProMonat * geldProStunde;
    }
}