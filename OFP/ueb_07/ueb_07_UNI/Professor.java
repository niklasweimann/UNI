/*
 * Created on Sun Jun 10 2018 at 23:4:12
 * @author Niklas Weimann
 * Matrikelnummer: 1352285
 */
/**
 * Professor
 */
public class Professor extends Angestellter {
    public Professor(double pGeldProStunde) {
        super(160, pGeldProStunde);
    }
}