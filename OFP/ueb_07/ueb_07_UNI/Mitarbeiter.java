/*
 * Created on Sun Jun 10 2018 at 23:4:26
 * @author Niklas Weimann
 * Matrikelnummer: 1352285
 */
/**
 * Mitarbeiter
 */
public class Mitarbeiter extends Angestellter{
    public Mitarbeiter(double arbeitstdProMonat, double gehaltProStunde) {
        super(arbeitstdProMonat, gehaltProStunde);
    }
    
    public double berechneMonatseinkommen() {
        return (stdProMonat * geldProStunde) + 500;
    }
}