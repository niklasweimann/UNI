/*
 * Created on Sun Jun 10 2018 at 23:4:35
 * @author Niklas Weimann
 * Matrikelnummer: 1352285
 */
/**
 * Uni
 */
public class Uni {
    
    public static void main(String[] args) {
        Angestellter[] personal = new Angestellter[3];
        double summeEinkommen = 0;

        personal[0] = new Angestellter(22,11.5);
        personal[1] = new Professor(121.40);
        personal[2] = new Mitarbeiter(40, 15);

        for (Angestellter a : personal)
            summeEinkommen += a.berechneMonatseinkommen();
        System.out.println("Summe der Einkommen: " + summeEinkommen);
    }
}