/*
 * Created on Sun Jun 10 2018 at 23:4:48
 * @author Niklas Weimann
 * Matrikelnummer: 1352285
 */
class Buch extends Medium{
    private String Buch_Autor;
    private int Buch_Seiten;

    Buch(String in_titel, String in_autor, int in_seiten)
    {
        super(in_titel);
        Buch_Autor = in_autor;
        Buch_Seiten = in_seiten;
    }

    public double berechneStrafgeld(){
        Datum heute = new Datum();
        int daysDiff = heute.calcDiffDays(Ausleihdatum);

        if (daysDiff > 28)
            return (daysDiff - 28);
        else
            return 0.0;
    }

    public String getAutor(){
        return Buch_Autor;
    }

    public int getSeiten() {
        return Buch_Seiten;
    }
}