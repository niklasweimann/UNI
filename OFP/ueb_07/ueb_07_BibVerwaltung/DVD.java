/*
 * Created on Sun Jun 10 2018 at 23:5:4
 * @author Niklas Weimann
 * Matrikelnummer: 1352285
 */
class DVD extends Medium{
    private String DVD_Regisseur;
    private int DVD_Laenge;

    DVD(String in_titel, String in_regisseur, int in_laenge)
    {
        super(in_titel);
        DVD_Regisseur = in_regisseur;
        DVD_Laenge = in_laenge;
    }

    public double berechneStrafgeld() {
        Datum heute = new Datum();
        int daysDiff = heute.calcDiffDays(Ausleihdatum);

        if (daysDiff > 7)
            return (daysDiff - 7)*2;
        else
            return 0.0;
    }

    public String getRegisseur() {
        return DVD_Regisseur;
    }

    public int getLaenge() {
        return DVD_Laenge;
    }
}