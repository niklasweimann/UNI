/*
 * Created on Sun Jun 10 2018 at 23:4:58
 * @author Niklas Weimann
 * Matrikelnummer: 1352285
 */
import java.util.*;
import java.io.*;

public class Datum {
	private int Jahr;
	private int Monat;
	private int Tag;
	private Calendar cal;
	
	public Datum() {
		cal=Calendar.getInstance();
		Jahr=cal.get(1);
		Monat=cal.get(2)+1;
		Tag=cal.get(5);
	}	
	
	public Datum(int jahr, int monat, int tag){
		cal=Calendar.getInstance();
		this.Jahr=jahr;
		this.Monat=monat;
		this.Tag=tag;
   }
	
	public int getJahr() {
		return Jahr;
	}
	
	public int getMonat() {
		return Monat;
    }
    
    public int getTag() {
    	return Tag;
	}
	
	public int calcDiffDays(Datum ausleih) {
		int daysDiff = 0;
		//Ihre Loesung
		int ausleih_jahr = ausleih.getJahr();
		int ausleih_monat = ausleih.getMonat();
		int ausleih_tag = ausleih.getTag();

		//Unterschied berechnen
		daysDiff += (Jahr - ausleih_jahr) * 365;
		daysDiff += (Monat - ausleih_monat) * 30;
		daysDiff += Tag - ausleih_tag;

		return daysDiff;
	}
}