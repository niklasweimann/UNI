/*
 * Created on Sun Jun 10 2018 at 23:4:42
 * @author Niklas Weimann
 * Matrikelnummer: 1352285
 */
import java.io.*;

public class BibVerwaltung {
    public static void main(String args[]) {
        Buch mBuch = new Buch("Buch1", "Autor1", 5180);
        DVD mDVD = new DVD("DVD1", "Regisseur1", 90);

        Datum mDatum = new Datum(2018, 5, 5);

        mBuch.setAusleihdatum(mDatum);
        mDVD.setAusleihdatum(mDatum);

        //Ausgabe Ausleihdatum
        System.out.println("Ausleihdatum: " + mDatum.getJahr() + "." + mDatum.getMonat() + "." + mDatum.getTag());

        System.out.println("Buch: " + mBuch.getTitel() + " von " + mBuch.getAutor() + " mit " + mBuch.getSeiten() + " Seiten.");
        System.out.println("Strafgeld fuer Buch: " + mBuch.berechneStrafgeld());
        System.out.println("DVD: " + mDVD.getTitel() + " von " + mDVD.getRegisseur() + " mit " + mDVD.getLaenge() + " Spielminuten.");
        System.out.println("Strafgeld fuer DVD: " + mDVD.berechneStrafgeld());
    }
}