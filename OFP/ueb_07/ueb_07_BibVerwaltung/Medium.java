/*
 * Created on Sun Jun 10 2018 at 23:5:9
 * @author Niklas Weimann
 * Matrikelnummer: 1352285
 */
abstract class Medium{
	protected String Mediumtitel;
	protected Datum Ausleihdatum;

	Medium(String titel)
	{
		this.Mediumtitel = titel;
	}
	protected String getTitel()
	{
		return Mediumtitel;
	}

	protected abstract double berechneStrafgeld();
	
	protected  void setAusleihdatum(Datum leihdatum)
	{
		this.Ausleihdatum = leihdatum;
	}
}