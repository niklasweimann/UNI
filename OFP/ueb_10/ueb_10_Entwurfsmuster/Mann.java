public class Mann extends Person{

    public Mann(String name, String gender) {
        this.name = name;
        this.gender = gender;
    }

    @Override
    protected String getGruss() {
        return "Hallo Herr " + this.name;
    }
}