public class Frau extends Person{

    public Frau(String name, String gender) {
        this.name = name;
        this.gender = gender;
    }

    @Override
    protected String getGruss() {
        return "Hallo Frau " + this.name;
    }
}