﻿public class GrussFactory extends abstractGrussFactory{

    @Override
    public Person createGruss(String name, String gender) {
        switch (gender) {
            case "M":
            case "m":
                return (new Mann(name, gender));
            case "F":
            case "f":
                return (new Frau(name, gender));
            default:
                System.out.println("Das angegebene Geschlecht konnte nicht erkannt werden. Bitte wählen Sie M oder F.");
                return (new Person());
        }
    }
}