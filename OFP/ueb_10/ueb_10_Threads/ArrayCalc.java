public class ArrayCalc {
    private int[] mArray;
    private int sum = 0;
    private Thread mThread;

    public ArrayCalc(int[] pArray, int pStart, int pEnd) {
        this.mArray = pArray;
        mThread = new Thread(() -> berechne(pStart, pEnd));
    }
    
    public synchronized void start() {
        mThread.start();
    }
    
    public synchronized void stop() {
        try {
            mThread.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void berechne(int start, int end) {
        for (int i = start; i <= end; i++) {
            sum += mArray[i];
        }
    }
    
    public int getSum() {
        return this.sum;
    }
}